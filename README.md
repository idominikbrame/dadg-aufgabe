# Follow below steps to start the dadg-aufgabe project

# Instructions for starting Front-End (be sure you're in /frontend directory)

### 1. Install packages: 
```sh
npm install
```


### 2. Compile and Hot-Reload for Development:

```sh
npm run dev
```
### 3. Once compiled navigate to url localhost:5173 in Browser (Chrome, Edge, Firefox, ...rest)
<br>

# Instructions for starting docker container for MongoDB

### 1. Start docker shell:
```sh
 sudo systemctl start docker
```

##### <----- (NOTE MAY BE DIFFERENT ON YOUR SYSTEM) ----->


### 2. Run docker container with the following command:
```sh
 docker run --name dadg-aufgabe-dominik -p 27017:27017 -d mongo
```
<br>

# Instructions for starting Back-End (be sure you're in /backend directory)

### 1. Install packages:
```sh
npm install
```

### 2. Compile and Hot-Reload for Development

```sh
npm start
```

### This will do the following:
- Start Express Server
- Connect to MongoDB
- Add data pulled manually from dealership (Kia & Seat) webpages to DB
- Log example data pulled through web scraping

# App Usage:
- Once you navigate to url and have started server/connected to db you can search either via pzl (e.g. 92676) or you can search by city name. The search bar will automatically suggest available zip codes and cities for you. If no match is found for search criteria it will return a < no matches found > message.
- Search results can be exported a .xlsx file extensions and saved locally to be viewed as Excel Spread Sheets