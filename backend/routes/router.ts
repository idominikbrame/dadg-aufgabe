import {Request, Response} from "express";
import DealersModel from "../schemas/dealer.schema";

const express = require("express");
const router = express.Router();

//returns all dealers
router.get('/getAllDealers', async (req: Request, res: Response) => {
    try {
        let dealers = await DealersModel.find({}, )
        res.json(dealers);
    } catch(err) {
            console.log(err)
    }
})

export default router
