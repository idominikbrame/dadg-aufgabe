import * as mongoose from "mongoose";
import cors from 'cors'
import express, {Express, Request, Response, Application, json} from 'express';
import router from './routes/router'
import {initDataToDB} from "./helpers/initDataToDB";
import {kiaScraper, opelScraper} from "./helpers/dealer.WebsiteScraper";

//Express Setup
const app: Application = express();
const port = process.env.PORT || 3005;

//Middleware
app.use(express.urlencoded({extended: true}));
app.use(express.json())
app.use(cors());
app.use('/', router)

//Start Server
app.listen(port, () => {
    console.log(`Server is ongoing at http://localhost:${port}`);
});

//Connect to MONGODB locally
mongoose.connect('mongodb://127.0.0.1:27017/dadg-app')
    .then(() => console.log('connected to mongodb'))
    .then(() => initDataToDB())
    .catch(error => console.error(error));


// kiaScraper()
opelScraper() /* Used to WebScrape -> Issues with cookies for Kia/Seat but was able to pull data from opel based on Zip Code and log data to the console */
// All other data was pulled from network tab for the sake of finishing the task

