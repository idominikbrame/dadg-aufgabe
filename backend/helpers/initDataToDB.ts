import DealersModel from "../schemas/dealer.schema";
import DealerSchema from "../schemas/dealer.schema";
import {dataAggregation} from "./dealer.DataAggregator";

//Add Data to DB if it does not already exist
export async function initDataToDB(): Promise<void> {
    await DealerSchema.countDocuments()
        .then(async dealersCount => {
           if(!dealersCount) {
               const newDealer  = new DealersModel({
                   allDealers: dataAggregation()
               })

               return await newDealer.save()
           }
        })
        .catch((err) => console.error(err))
    return
}