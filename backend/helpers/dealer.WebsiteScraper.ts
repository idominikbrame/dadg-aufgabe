// TODO -> This file will be used to attempt web scraping on both Seat.de and Kia.de to retrieve data that way.
// @ts-ignore
import * as puppeteer from 'puppeteer'; // or import puppeteer from 'puppeteer-core';
export async function opelScraper() {


// Launch the browser and open a new blank page
    const browser = await puppeteer.launch();
    const page = await browser.newPage();

// Navigate the page to a URL.
    await page.goto('https://www.opel.de/tools/haendlersuche.html');

    await page.setViewport({width: 1080, height: 1024});
    const cookieID = '#_psaihm_refuse_all';
    await page.waitForSelector(cookieID);
    await page.click(cookieID)

// Find dealer serach input
    const searchResultSelector = '.dealer-search-box';
    await page.waitForSelector(searchResultSelector);

// Type into search box.
    await page.type('.dealer-search-box', '92676');

// Wait and click on search
    const searchClassButton = '.dealer-search-button';
    await page.waitForSelector(searchClassButton);
    await page.click(searchClassButton);

// Find dealer list div and map over it to get over first item and use variable object 'data' to store values
    const dealerList = '.dealer-list';
    await page.waitForSelector(dealerList);
    const data = await page.$$eval(".dealer-list", els => els.map(el => ({
        name: el?.querySelector(".q-dealer-name")?.textContent,
        street: el?.querySelector(".q-dealer-name")?.textContent,
        cityPLZ: el?.querySelector(".q-dealer-name")?.textContent,
        phone: el?.querySelector(".q-phone > a > span")?.textContent,
        website: el?.querySelector(".q-contact-url")?.textContent,
    })));


    console.log(data);
    await browser.close();
    return data;
}

export async function kiaScraper() {


// Launch the browser and open a new blank page
    const browser = await puppeteer.launch();
    const page = await browser.newPage();

// Navigate the page to a URL.
    await page.goto('https://www.kia.com/de/haendlersuche/#/');

    await page.setRequestInterception(true);

    await page.setViewport({width: 1080, height: 1024});
    const cookieBanner = '#onetrust-banner-sdk';
    await page.waitForSelector(cookieBanner);
    await page.waitForSelector("#onetrust-button-group");
    await page.waitForSelector("#onetrust-accept-btn-handler")
    await page.click("#onetrust-accept-btn-handler");

    await page.screenshot({
        path: 'png.png'
    })

    await browser.close();
    return;
}
