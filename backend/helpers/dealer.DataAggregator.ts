import Dealer from "../classes/dealer.Class";
import {kiaDealersRaw} from "../data/kiaDealersRaw";
import {seatDealersRaw} from "../data/seatDealersRaw";


//Handler to aggregate necessary data for Database for Seat
export function dataAggregation() {
    const dataAggregationArray: any[] = [];

    // Loop Through Raw Data Files and push minified aggregated data
    for (let i = 0; i < seatDealersRaw.length; i++) {
        let newDealer: Dealer = new Dealer(
            {
                city: seatDealersRaw[i].ORT || '',
                plz: seatDealersRaw[i].PLZ || '',
                street: seatDealersRaw[i].STRAßE || ''
            },
            seatDealersRaw[i].NUMMER || '',
            seatDealersRaw[i].EMAIL || '',
            seatDealersRaw[i].FAX || '',
            seatDealersRaw[i].YPOS || '',
            seatDealersRaw[i].XPOS || '',
            seatDealersRaw[i].NAME1 || '',
            seatDealersRaw[i].TELEFON || '',
            seatDealersRaw[i].URL || '',
            "seat"
        )

        dataAggregationArray.push(newDealer);
    }

    for (let i = 0; i < kiaDealersRaw.length; i++) {
        let newDealer: Dealer = new Dealer(
            {
                city: kiaDealersRaw[i].dealerResidence,
                plz: kiaDealersRaw[i].dealerPostcode,
                street: kiaDealersRaw[i].dealerAddress
            },
            kiaDealersRaw[i].dealerSeq || '',
            kiaDealersRaw[i].contactEmail || '',
            kiaDealersRaw[i].dealerFax || '',
            kiaDealersRaw[i].dealerLatitude || '',
            kiaDealersRaw[i].dealerLongitude || '',
            kiaDealersRaw[i].dealerName || '',
            kiaDealersRaw[i].dealerPhone || '',
            kiaDealersRaw[i].websiteUrl || '',
            "kia"
        )

        dataAggregationArray.push(newDealer);
    }

    return dataAggregationArray;
}


