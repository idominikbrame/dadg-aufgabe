import mongoose, { Schema, model, connect } from 'mongoose';

// Dealer schema for mongodb structure -> not completely necessary in this small task but added anyway
const dealerSchema = new Schema({
    allDealers: []
})

// Model to be used for querying and updating/retrieving data database
const DealersModel = mongoose.model('Dealers', dealerSchema);
export default DealersModel;