
// Dealer class used to construct aggregated data from <dealer>Dealer.ts files.
export default class Dealer {
    address: IAddress;
    dealerNumber: string;
    email: string;
    fax: string;
    latitude: string;
    longitude: string;
    name: string;
    phone: string;
    website: string;
    dealer: string

    constructor(
        address: IAddress,
        dealerNumber: string,
        email: string,
        fax: string,
        latitude: string,
        longitude: string,
        name: string,
        phone: string,
        website: string,
        dealer: string
    ) {
        this.address = address;
        this.dealerNumber = dealerNumber;
        this.email = email;
        this.fax = fax;
        this.latitude = latitude;
        this.longitude = longitude;
        this.name = name;
        this.phone = phone;
        this.website = website;
        this.dealer = dealer;
    }
}

interface IAddress {
    street: string;
    plz: string;
    city: string;
}